package com.codegreenllc.nohingo.studentservice.controllers;

import static org.springframework.http.ResponseEntity.ok;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codegreenllc.nohingo.studentservice.security.jwt.CustomUserDetails;

@RestController
@RequestMapping("/v1/students")
public class StudentController {

	@GetMapping("")
	public ResponseEntity<String> all() {
		final CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		System.out.println(userDetails);
		return ok("hello " + userDetails.getNickname());
	}
}
