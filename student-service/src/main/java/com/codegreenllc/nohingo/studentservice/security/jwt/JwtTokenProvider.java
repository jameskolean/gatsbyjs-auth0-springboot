package com.codegreenllc.nohingo.studentservice.security.jwt;

import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.auth0.jwk.InvalidPublicKeyException;
import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.JwkProviderBuilder;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Verification;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class JwtTokenProvider {

	@Value("${jwt.issuer}")
	private String issuer;

	private Algorithm getAlgorythm(final DecodedJWT decodedJwt) throws JwkException, InvalidPublicKeyException {
		final JwkProvider jwkProvider = new JwkProviderBuilder(issuer).build();
		final Jwk jwk = jwkProvider.get(decodedJwt.getKeyId());
		final Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);
		return algorithm;
	}

	public Authentication getAuthentication(final String token) {
		final Map<String, Claim> claims = JWT.decode(token).getClaims();
		final UserDetails userDetails = CustomUserDetails.builder() //
				.accountNonExpired(true)//
				.accountNonLocked(true)//
				.credentialsNonExpired(true)//
				.email(claims.get("email").asString())//
				.enabled(true)//
				.nickname(claims.get("nickname").asString())//
				.picture(claims.get("picture").asString())//
				.username(claims.get("name").asString())//
				.build();

		return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
	}

	public String getUsername(final String token) {
		return JWT.decode(token).getSubject();
	}

	public String resolveToken(final HttpServletRequest req) {
		final String bearerToken = req.getHeader("Authorization");
		if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

	public boolean validateToken(final String token) {
		try {
			final DecodedJWT decodedJwt = JWT.decode(token);
			final Verification verifier = JWT.require(getAlgorythm(decodedJwt));
			final DecodedJWT verify = verifier.build().verify(decodedJwt);
			if (verify.getClaim("exp").asDate().before(new Date())) {
				log.info("Expired JWT token.");
				return false;
			}
		} catch (final JwkException e) {
			throw new InvalidJwtAuthenticationException("Expired or invalid JWT token.", e);
		}
		return true;
	}

}
