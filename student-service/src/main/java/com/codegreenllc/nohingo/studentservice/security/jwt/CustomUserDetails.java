package com.codegreenllc.nohingo.studentservice.security.jwt;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.ToString;

@ToString
@Builder
public class CustomUserDetails implements UserDetails {

	private static final long serialVersionUID = 1L;

	@Getter
	boolean accountNonExpired;
	@Getter
	boolean accountNonLocked;
	@Getter
	@Default
	Collection<? extends GrantedAuthority> authorities = new HashSet<>();
	@Getter
	private final boolean credentialsNonExpired;
	@Getter
	String email;
	@Getter
	private final boolean enabled;
	@Getter
	String nickname;
	@Getter
	String picture;
	@Getter
	String username;

	@Override
	public String getPassword() {
		return null;
	}
}
