package com.codegreenllc.nohingo.studentservice.security.jwt;

import org.springframework.security.core.AuthenticationException;

public class InvalidJwtAuthenticationException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public InvalidJwtAuthenticationException(final String e, final Throwable t) {
		super(e, t);
	}
}
