package com.codegreenllc.nohingo.studentservice;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.codegreenllc.nohingo.studentservice.security.jwt.JwtSecurityConfigurer;
import com.codegreenllc.nohingo.studentservice.security.jwt.JwtTokenProvider;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Value("${cors.allowed.origins}")
	private String[] allowedOrigins;

	@Autowired
	JwtTokenProvider jwtTokenProvider;

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http.httpBasic().disable() //
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and() //
				.authorizeRequests() //
				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll() //
				.anyRequest().authenticated().and() //
				.apply(new JwtSecurityConfigurer(jwtTokenProvider));
		http.cors(); // looks for bean CorsConfigurationSource
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		final CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(Arrays.asList(allowedOrigins));
		configuration.setAllowedMethods(Arrays.asList("GET", "POST"));
		configuration.setAllowCredentials(true);
		configuration.addAllowedHeader("Authorization");
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}

}
