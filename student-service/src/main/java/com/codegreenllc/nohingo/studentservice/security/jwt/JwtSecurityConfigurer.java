package com.codegreenllc.nohingo.studentservice.security.jwt;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JwtSecurityConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

	private final JwtTokenProvider jwtTokenProvider;

	public JwtSecurityConfigurer(final JwtTokenProvider jwtTokenProvider) {
		this.jwtTokenProvider = jwtTokenProvider;
	}

	@Override
	public void configure(final HttpSecurity http) throws Exception {
		final JwtTokenAuthenticationFilter customFilter = new JwtTokenAuthenticationFilter(jwtTokenProvider);
		http.exceptionHandling().authenticationEntryPoint(new JwtAuthenticationEntryPoint()).and()
				.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
	}
}
