package com.codegreenllc.nohingo.studentservice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.auth0.jwk.JwkException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentServiceApplicationTests {
//	JWT_TOKEN_KEY
	@Test
	public void contextLoads() throws JwkException {
//		final String jwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik1qTkVRMFV6TjBaR1FUa3hRamxETlVNNU9VRTNOVEpFTURrMlFrSkRRVEpCUkVFNE5rSkJSQSJ9.eyJuaWNrbmFtZSI6ImphbWVza29sZWFuIiwibmFtZSI6ImphbWVza29sZWFuQGdtYWlsLmNvbSIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci8zMTZkY2RhNGU1NDRjYWYxNDcyNTM3MzYwMDdkZGQ0Mz9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRmphLnBuZyIsInVwZGF0ZWRfYXQiOiIyMDE5LTA5LTE2VDEzOjMwOjE5Ljg0NVoiLCJlbWFpbCI6ImphbWVza29sZWFuQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiaXNzIjoiaHR0cHM6Ly9jb2RlZ3JlZW5sbGMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDViZTViZWIwYTkwYzc1MzEwYmYwYTliYSIsImF1ZCI6InlqNFBHMnRhd0FWY2NJcXpadXhlS3J6aVgzY1JWaDFzIiwiaWF0IjoxNTY4NjQxMzk3LCJleHAiOjE1Njg2NzczOTcsImF0X2hhc2giOiJZbnB5WExkU3JXUUMxbEQ1TjA3eTdBIiwibm9uY2UiOiJBNGZJeHB5LjNjVE93MTdPLWo2OFJWS3hObG44SnphMiJ9.xklmVGhvz3W_N8wBcZmcWMoWMOhgXrn7H4RWWTFXCxGhTqOWRo3qAhu_4FMWNPSAYjhhghpo-dmz4Qilj0tBNPN7JckktedguUjHaYRzSo6KaQ-J_6Ax0BCzNkZy3Yuu87aab6KqMy5w0ZYyYikyzMs0Ezb0jN4V8lEIejEYQabTM_ZtTQuil1KI1VX1kNTzKgEtS_VdxRhscLiJjD1gBvOd9dx69dby8XqhkaVtTc2ewFEhCBwShMRp1XzzPh2evPduuY0kh4gW4vjr7S0evasjNGoUS6yMloqHv-OHL7o40YaZz73jhMurVu6d53-Ekc6iYdp2UrZWOrCJ1plm4Q";
//		final String accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik1qTkVRMFV6TjBaR1FUa3hRamxETlVNNU9VRTNOVEpFTURrMlFrSkRRVEpCUkVFNE5rSkJSQSJ9.eyJuaWNrbmFtZSI6ImphbWVza29sZWFuIiwibmFtZSI6ImphbWVza29sZWFuQGdtYWlsLmNvbSIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci8zMTZkY2RhNGU1NDRjYWYxNDcyNTM3MzYwMDdkZGQ0Mz9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRmphLnBuZyIsInVwZGF0ZWRfYXQiOiIyMDE5LTA5LTE2VDEzOjMwOjE5Ljg0NVoiLCJlbWFpbCI6ImphbWVza29sZWFuQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiaXNzIjoiaHR0cHM6Ly9jb2RlZ3JlZW5sbGMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDViZTViZWIwYTkwYzc1MzEwYmYwYTliYSIsImF1ZCI6InlqNFBHMnRhd0FWY2NJcXpadXhlS3J6aVgzY1JWaDFzIiwiaWF0IjoxNTY4NjQxMzk3LCJleHAiOjE1Njg2NzczOTcsImF0X2hhc2giOiJZbnB5WExkU3JXUUMxbEQ1TjA3eTdBIiwibm9uY2UiOiJBNGZJeHB5LjNjVE93MTdPLWo2OFJWS3hObG44SnphMiJ9.xklmVGhvz3W_N8wBcZmcWMoWMOhgXrn7H4RWWTFXCxGhTqOWRo3qAhu_4FMWNPSAYjhhghpo-dmz4Qilj0tBNPN7JckktedguUjHaYRzSo6KaQ-J_6Ax0BCzNkZy3Yuu87aab6KqMy5w0ZYyYikyzMs0Ezb0jN4V8lEIejEYQabTM_ZtTQuil1KI1VX1kNTzKgEtS_VdxRhscLiJjD1gBvOd9dx69dby8XqhkaVtTc2ewFEhCBwShMRp1XzzPh2evPduuY0kh4gW4vjr7S0evasjNGoUS6yMloqHv-OHL7o40YaZz73jhMurVu6d53-Ekc6iYdp2UrZWOrCJ1plm4Q";
//
//		final String issuer = "https://codegreenllc.auth0.com/";
//
//		final DecodedJWT decodedJwt = JWT.decode(accessToken);
//		final JwkProvider jwkProvider = new JwkProviderBuilder(issuer).build();
//		final Jwk jwk = jwkProvider.get(decodedJwt.getKeyId());
//		final Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);
//
//		final Verification verifier = JWT.require(algorithm);
//		final DecodedJWT verify = verifier.build().verify(decodedJwt);
//		System.out.println(verify.getPayload());
//		System.out.println(verify.getHeader());
//		System.out.println(verify.getSubject());
//		final Map<String, Claim> claims = verify.getClaims();
////		claims.entrySet().stream().forEach((e) -> System.out.println(e.getKey() + ":" + e.getValue().asString()));
//
////		final CustomUserDetails user = new CustomUserDetails(claims.get("name").asString(),
////				claims.get("nickname").asString(), claims.get("email").asString(), claims.get("picture").asString());
////		System.out.println(user);
	}

}
