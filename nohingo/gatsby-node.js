/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

exports.createPages = async ({ actions, graphql, reporter }) => {
  const result = await graphql(`
    {
      allContentfulCourse(filter: { node_locale: { eq: "en-US" } }) {
        nodes {
          slug
        }
      }
      allContentfulLesson(filter: { node_locale: { eq: "en-US" } }) {
        nodes {
          slug
        }
      }
    }
  `)
  if (result.errors) {
    reporter.panic('Error Loading Courses', result.errors)
  }
  result.data.allContentfulCourse.nodes.forEach(course => {
    actions.createPage({
      path: `/courses/${course.slug}`,
      component: require.resolve('./src/templates/course-template.js'),
      context: {
        slug: course.slug,
      },
    })
  })
  result.data.allContentfulLesson.nodes.forEach(lesson => {
    actions.createPage({
      path: `/lesson/${lesson.slug}`,
      component: require.resolve('./src/templates/lesson-template.js'),
      context: {
        slug: lesson.slug,
      },
    })
  })
}

// Implement the Gatsby API “onCreatePage”. This is
// called after every page is created.
exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions

  // page.matchPath is a special key that's used for matching pages
  // only on the client.
  if (page.path.match(/^\/student/)) {
    page.matchPath = '/student/*'

    // Update the page.
    createPage(page)
  }
}

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === 'build-html') {
    /*
     * During the build step, `auth0-js` will break because it relies on
     * browser-specific APIs. Fortunately, we don’t need it during the build.
     * Using Webpack’s null loader, we’re able to effectively ignore `auth0-js`
     * during the build. (See `src/utils/auth.js` to see how we prevent this
     * from breaking the app.)
     */
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /auth0-js/,
            use: loaders.null(),
          },
        ],
      },
    })
  }
}
