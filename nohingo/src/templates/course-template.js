import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import SEO from '../components/seo'

export const query = graphql`
  query($slug: String!) {
    course: contentfulCourse(slug: { eq: $slug }) {
      slug
      name
      description {
        description
      }
    }
  }
`
const CourseTemplate = ({ data: { course } }) => (
  <Layout>
    <SEO title={course.name} />

    <p>
      <h1>{course.name}</h1>
      <p>{course.description.description}</p>
    </p>
  </Layout>
)

export default CourseTemplate
