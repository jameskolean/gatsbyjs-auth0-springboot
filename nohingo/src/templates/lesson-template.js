import React from 'react'
import PropTypes from 'prop-types'
import { Player } from 'video-react'
import ReactAudioPlayer from 'react-audio-player'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import SEO from '../components/seo'
import '../../node_modules/video-react/dist/video-react.css' // import css

export const query = graphql`
  query($slug: String!) {
    lesson: contentfulLesson(slug: { eq: $slug }) {
      id
      slug
      title
      sections {
        ... on ContentfulAudioSection {
          id
          caption
          audio {
            id
            file {
              url
            }
          }
        }
        ... on ContentfulVideoSection {
          id
          caption
          video {
            id
            file {
              url
            }
          }
        }
      }
    }
  }
`
const Sections = ({ sections }) => {
  if (!sections || !sections.length === 0)
    return <h1>Please add at least one section</h1>
  return (
    <>
      {sections.map((section, index) => {
        if (section.video) {
          const videoUrl = `https:${section.video.file.url}`
          return <Player key={section.video.id} playsInline src={videoUrl} />
        }
        if (section.audio) {
          const audioUrl = `https:${section.audio.file.url}`
          return <ReactAudioPlayer controls src={audioUrl} />
        }
        return <h1 key={index}>unknown component</h1>
      })}
    </>
  )
}
Sections.propTypes = {
  sections: PropTypes.array,
}

const LessonTemplate = ({ data: { lesson } }) => (
  <Layout>
    <SEO title={lesson.title} />
    <h1>{lesson.title}</h1>
    <Sections sections={lesson.sections} />
  </Layout>
)

LessonTemplate.propTypes = {
  data: PropTypes.shape({
    lesson: PropTypes.shape({
      title: PropTypes.string,
      sections: PropTypes.array,
    }),
  }),
}

export default LessonTemplate
