import React from 'react'
import { Link, graphql } from 'gatsby'
import Layout from '../components/layout'
import SEO from '../components/seo'

export const query = graphql`
  {
    courses: allContentfulCourse(filter: { node_locale: { eq: "en-US" } }) {
      nodes {
        slug
        name
        code
      }
    }
  }
`
const IndexPage = ({ data }) => (
  <Layout>
    <SEO title="Home" />
    <h1>NohinGo</h1>
    <p>Welcome to NohinGo.</p>
    <p>The fun way to instruct and learn.</p>
    {data.courses.nodes.map(course => (
      <div key={`course-${course.slug}`}>
        <h5>
          {course.code} - {course.name}
        </h5>
      </div>
    ))}

    <Link to="/student/">Student Page</Link>
  </Layout>
)

export default IndexPage
