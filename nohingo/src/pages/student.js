// This is student specific section of the app. It is authenticated and runs on the client
import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import axios from 'axios'
import useAxios from 'axios-hooks'
import { Router } from '@reach/router'
import Layout from '../components/layout'
import { login, isAuthenticated, getToken } from '../utils/auth'

const Dashboard = () => <p>This is the student dashboard</p>
const Courses = ({ children }) => {
  axios.defaults.headers.common.Authorization = `Bearer ${getToken()}`
  const [{ data, loading, error }, refetch] = useAxios(
    'http://localhost:8080/v1/students'
  )
  if (loading) return <p>Loading...</p>
  if (error) return <p>Error!</p>
  console.log()
  return (
    <div>
      <h2>Courses</h2>
      <button onClick={refetch}>refetch</button>
      <pre>{JSON.stringify(data, null, 2)}</pre>
      {children}
    </div>
  )
}

// const Courses = ({ children }) => (
//   <div>
//     <h2>Courses</h2>
//     <ul>
//       <li>
//         <Link to="/student/courses/ALG-101">Linear Algerbria 101</Link>
//       </li>
//       <li>
//         <Link to="/student/courses/PHY-101">Physics 101</Link>
//       </li>
//     </ul>
//     {children}
//   </div>
// )

const Course = ({ courseId }) => (
  <div>
    <h2>Course {courseId}</h2>
  </div>
)
Course.propTypes = {
  courseId: PropTypes.string,
}

const CourseIndex = () => (
  <div>
    <p>Please choose a course.</p>
  </div>
)
const Student = () => {
  if (!isAuthenticated()) {
    login()
    return <p>Redirecting to login...</p>
  }

  return (
    <>
      <Layout>
        <nav>
          <Link to="/student/">My Dashboard</Link> <br />
          <Link to="/student/courses">My Courses</Link>
          <br />
        </nav>
        <Router>
          <Dashboard path="/student/*" />
          <Courses path="/student/courses">
            <CourseIndex path="/" />
            <Course path=":courseId" />
          </Courses>
        </Router>
      </Layout>
    </>
  )
}

export default Student
