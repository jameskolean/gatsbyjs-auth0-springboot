import React from 'react'
import { handleAuthentication } from '../utils/auth'

const AuthCallback = () => {
  handleAuthentication()

  return <p>Loading...</p>
}

export default AuthCallback
