import auth0 from 'auth0-js'
import { navigate } from 'gatsby'

export const isBrowser = typeof window !== 'undefined'
const auth = isBrowser
  ? new auth0.WebAuth({
      domain: process.env.GATSBY_AUTH0_DOMAIN,
      clientID: process.env.GATSBY_AUTH0_CLIENTID,
      redirectUri: process.env.GATSBY_AUTH0_CALLBACK,
      responseType: 'token id_token',
      scope: 'openid profile email',
    })
  : {}

const tokens = {
  idToken: false,
  accessToken: false,
  expiresAt: false,
}

const user = {}

export const isAuthenticated = () => {
  if (!isBrowser) {
    return
  }
  const isTokenExpired =
    !tokens.expiresAt || tokens.expiresAt < new Date().getTime()
  return localStorage.getItem('isLoggedIn') === 'true' && !isTokenExpired
}

export const login = () => {
  if (!isBrowser) {
    return
  }
  auth.authorize()
}
const setSession = (cb = () => {}) => (err, authResult) => {
  if (err) {
    navigate('/')
    cb()
    return
  }
  if (authResult && authResult.accessToken && authResult.idToken) {
    const expiresAt = authResult.expiresIn * 1000 + new Date().getTime()
    tokens.accessToken = authResult.accessToken
    tokens.idToken = authResult.idToken
    tokens.expiresAt = expiresAt

    auth.client.userInfo(tokens.accessToken, (_err, userProfile) => {
      user.nickname = userProfile.nickname
      user.name = userProfile.name
      user.picture = userProfile.picture
      window.localStorage.setItem('isLoggedIn', true)

      localStorage.setItem('isLoggedIn', true)
      navigate('/student')

      cb()
    })
  }
}

export const silentAuth = callback => {
  if (!isAuthenticated()) return callback()
  auth.checkSession({}, setSession(callback))
}

export const handleAuthentication = callback => {
  if (!isBrowser) {
    return
  }

  auth.parseHash(setSession(callback))
}

export const logout = () => {
  tokens.accessToken = false
  tokens.idToken = false
  tokens.expireAt = false
  user.name = false
  user.nickname = false
  user.picture = false
  window.localStorage.setItem('isLoggedIn', false)

  auth.logout()
}

export const isProtectedRoute = () => {
  const protectedRoutes = [`/student`, `/courses`]
  return protectedRoutes
    .map(route => window.location.pathname.includes(route))
    .some(route => route)
}

export const getProfile = () => user
export const getToken = () => tokens.idToken
