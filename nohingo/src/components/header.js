import { Link } from 'gatsby'
import PropTypes from 'prop-types'
import React from 'react'
import { login, logout, isAuthenticated, getProfile } from '../utils/auth'

const User = () => {
  if (isAuthenticated()) {
    const user = getProfile()
    return (
      <>
        <li>{user.name}</li>
        <li>
          <button
            type="button"
            href="#logout"
            onClick={e => {
              logout('')
              e.preventDefault()
            }}
          >
            Log Out
          </button>
        </li>
      </>
    )
  }
  return (
    <>
      <li>
        <button
          type="button"
          href="#login"
          onClick={e => {
            login()
            e.preventDefault()
          }}
        >
          Log In
        </button>
      </li>
    </>
  )
}
const Header = ({ siteTitle }) => (
  <header
    style={{
      background: `rebeccapurple`,
      marginBottom: `1.45rem`,
    }}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
      }}
    >
      <h1 style={{ margin: 0 }}>
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          {siteTitle}
        </Link>
      </h1>
      <ul className="userinfo">
        <User />
      </ul>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
