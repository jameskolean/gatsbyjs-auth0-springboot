/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */
import React, { useState, useEffect } from 'react'
import { silentAuth } from './src/utils/auth'

// Try to renew the session when the page reloads
const SessionCheck = ({ children }) => {
  const [isLoading, setIsLoading] = useState(true)
  useEffect(() => silentAuth(() => setIsLoading(false)))
  return isLoading === false && <>{children}</>
}

export const wrapRootElement = ({ element }) => (
  <SessionCheck>{element}</SessionCheck>
)
